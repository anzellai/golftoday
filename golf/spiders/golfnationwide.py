# -*- coding: utf-8 -*-
import scrapy
from golf.items import GolfItem


class GolfnationwideSpider(scrapy.Spider):
    name = "golfnationwide"
    allowed_domains = ["golfnationwide.com"]
    base_url = 'http://www.golfnationwide.com'
    start_urls = [
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Alabama-Golf-Courses__AL.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Kentucky-Golf-Courses__KY.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/North-Dakota-Golf-Courses__ND.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Alaska-Golf-Courses__AK.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Louisiana-Golf-Courses__LA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Ohio-Golf-Courses__OH.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Arizona-Golf-Courses__AZ.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Maine-Golf-Courses__ME.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Oklahoma-Golf-Courses__OK.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Arkansas-Golf-Courses__AR.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Maryland-Golf-Courses__MD.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Oregon-Golf-Courses__OR.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/California-Golf-Courses__CA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Massachusetts-Golf-Courses__MA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Pennsylvania-Golf-Courses__PA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Colorado-Golf-Courses__CO.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Michigan-Golf-Courses__MI.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Rhode-Island-Golf-Courses__RI.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Connecticut-Golf-Courses__CT.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Minnesota-Golf-Courses__MN.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/South-Carolina-Golf-Courses__SC.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Delaware-Golf-Courses__DE.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Mississippi-Golf-Courses__MS.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/South-Dakota-Golf-Courses__SD.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/District-Of-Columbia-Golf-Courses__DC.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Missouri-Golf-Courses__MO.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Tennessee-Golf-Courses__TN.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Florida-Golf-Courses__FL.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Montana-Golf-Courses__MT.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Texas-Golf-Courses__TX.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Georgia-Golf-Courses__GA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Nebraska-Golf-Courses__NE.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Utah-Golf-Courses__UT.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Hawaii-Golf-Courses__HI.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Nevada-Golf-Courses__NV.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Vermont-Golf-Courses__VT.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Idaho-Golf-Courses__ID.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/New-Hampshire-Golf-Courses__NH.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Virginia-Golf-Courses__VA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Illinois-Golf-Courses__IL.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/New-Jersey-Golf-Courses__NJ.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Washington-Golf-Courses__WA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Indiana-Golf-Courses__IN.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/New-Mexico-Golf-Courses__NM.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/West-Virginia-Golf-Courses__WV.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Iowa-Golf-Courses__IA.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/New-York-Golf-Courses__NY.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Wisconsin-Golf-Courses__WI.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Kansas-Golf-Courses__KS.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/North-Carolina-Golf-Courses__NC.aspx',
        u'http://www.golfnationwide.com/Golf-Courses-By-State/Wyoming-Golf-Courses__WY.aspx']

    def parse(self, response):
        links = response.xpath('//table[@id="ctl00_MainContentPlaceholder_GridView1"]//a/@href').extract()
        urls = [self.base_url+link for link in links]
        for url in urls:
            yield scrapy.Request(url, callback=self.parse2)

    def parse2(self, response):
        item = GolfItem()
        item['name'] = response.url.split('/')[-1].split('__')[0].replace('-', ' ')
        item['state'] = response.url.split('http://www.golfnationwide.com/')[1].split('/')[1]
        text = response.xpath('//span[@id="Block"]//text()').extract()
        lines = [line.strip() for line in text if line.strip()]
        item['info'] = '\t'.join(lines)
        yield item
