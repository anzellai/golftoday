# -*- coding: utf-8 -*-
import scrapy
from golf.items import GolfItem


class GolftodaySpider(scrapy.Spider):
    name = "golftoday"
    allowed_domains = ["www.golftoday.co.uk"]
    """
    start_urls = (
        'http://www.golftoday.co.uk/clubhouse/coursedir/england.html',
        'http://www.golftoday.co.uk/clubhouse/coursedir/ireland.html',
        'http://www.golftoday.co.uk/clubhouse/coursedir/scotland.html',
        'http://www.golftoday.co.uk/clubhouse/coursedir/wales.html',
    )
    """
    start_urls = (
        'http://www.golftoday.co.uk/clubhouse/coursedir/ukindex.html',
    )

    """
    def parse(self, response):
        for node in response.xpath('//table[@class="lines"]//a/@href').extract():
            url = 'http://www.golftoday.co.uk/clubhouse/coursedir/'+node.split('/')[-1]
            print 'parser 1: ', url
            yield scrapy.Request(url, callback=self.parse2)
    """
    def parse(self, response):
        exclude_urls = [
            'http://www.golftoday.co.uk/clubhouse/coursedir/england.html',
            'http://www.golftoday.co.uk/clubhouse/coursedir/ireland.html',
            'http://www.golftoday.co.uk/clubhouse/coursedir/scotland.html',
            'http://www.golftoday.co.uk/clubhouse/coursedir/wales.html',
        ]
        for node in response.xpath('//table[@class="lines"]//a/@href').extract():
            if node[0] != '/':
                url = 'http://www.golftoday.co.uk/clubhouse/coursedir/'+node
            else:
                url = 'http://www.golftoday.co.uk'+node
            if url not in exclude_urls:
                print 'parser 1: ', url
                yield scrapy.Request(url, callback=self.parse2)
    """
    def parse2(self, response):
        for node in response.xpath('//table[@class="countypage"]//tr//td[1]//a/@href').extract():
            if node.startswith('/'):
                url2 = 'http://www.golftoday.co.uk' + node
            else:
                url2 = 'http://www.golftoday.co.uk/clubhouse/coursedir/' + node
            print 'parse 2: ', url2
            yield scrapy.Request(url2, callback=self.parse3)
    """
    def parse2(self, response):
        for node in response.xpath('//table[contains(@class, "courseguide") or contains(@class, "countypage")]//tr//td//a/@href').extract():
            if node.startswith('/'):
                url2 = 'http://www.golftoday.co.uk' + node
            else:
                url2 = 'http://www.golftoday.co.uk/clubhouse/coursedir/' + node
            print 'parse 2: ', url2
            yield scrapy.Request(url2, callback=self.parse3)

    def parse3(self, response):
        sel = response.xpath('//table[@class="courseguide"]')
        item = GolfItem()
        item['name'] = ''.join(sel.xpath('//h1//text()').extract())
        item['country'] = ''.join(response.xpath('.//ul[@id="crumbs"]/li[3]//text()').extract())
        item['county'] = ''.join(response.xpath('.//ul[@id="crumbs"]/li[4]//text()').extract())
        for tr in sel.xpath('./tr'):
            item['row'] = [''.join(td.xpath('.//text()').extract()).strip() for td in tr.xpath('./td')]
            yield item
        '''
        item['address'] = ''.join(sel.xpath('tr[2]/td[2]//text()').extract())
        item['phone'] = ''.join(sel.xpath('tr[3]/td[2]//text()').extract())
        item['fax'] = ''.join(sel.xpath('tr[4]/td[2]//text()').extract())
        item['website'] = ''.join(sel.xpath('tr[5]/td[2]//text()').extract())
        item['email'] = ''.join(sel.xpath('tr[6]/td[2]//text()').extract())
        item['holes'] = ''.join(sel.xpath('tr[7]/td[2]//text()').extract())
        item['yardage'] = ''.join(sel.xpath('tr[8]/td[2]//text()').extract())
        item['visitors'] = ''.join(sel.xpath('tr[9]/td[2]//text()').extract())
        item['societies'] = ''.join(sel.xpath('tr[13]/td[2]//text()').extract())
        item['location'] = ''.join(sel.xpath('tr[16]/td[2]//text()').extract())
        item['facilities'] = ''.join(sel.xpath('tr[18]/td[2]//text()').extract())
        yield item
        '''
